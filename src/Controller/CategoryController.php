<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/category", name="category")
 */

class CategoryController extends Controller
{
    /**
     * @Route("/ajouter", name="ajouter")
     */
    public function ajout()
    {
        $em = $this->getDoctrine()->getManager();
        $category = new Category();
        $category->setNom('romance');
        $em->persist($category);
        $em->flush();

        $category1 = new Category();
        $category1->setNom('action');
        $em->persist($category1);
        $em->flush();

        $category2 = new Category();
        $category2->setNom('thriller');
        $em->persist($category2);
        $em->flush();

        return $this->render('category/ajout.html.twig');
    }

    /**
     * @Route("/", name="afficher")
     */
    public function afficher(){
        $em = $this->getDoctrine()->getManager();
        $categorys = $em->getRepository(Category::class)->findAll();

        return $this->render('category/index.html.twig',
            ['categorys'=>$categorys]);
    }
}
