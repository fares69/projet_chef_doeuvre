<?php

namespace App\Controller;

use App\Entity\Acteur;
use App\Form\ActeurType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/acteur", name="acteur")
 */
class ActeurController extends Controller
{
    /**
     * @Route("/", name="affiche")
     */
    public function index()
    {
        $acteurs = $em = $this->getDoctrine()->getManager()->getRepository(Acteur::class)->findAll();
        return $this->render('acteur/index.html.twig', [
            'acteurs' => $acteurs,
        ]);
    }

    /**
     * @Route("/ajout", name="acteur_show,")
     */
    public function ajout(Request $request)
    {
        $acteur = new Acteur();
        $form = $this->createForm(ActeurType::class, $acteur)
            ->add('save', SubmitType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $acteur = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($acteur);
            $em->flush();
            return $this->redirect('/acteur');
        }

        return $this->render('acteur/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/modif/{id}", name="modif")
     */
    public function modif($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $acteur = $em->getRepository(Acteur::class)->find($id);

        $form = $this->createForm(ActeurType::class, $acteur)
            ->add('save', SubmitType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $acteur = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirect('/acteur');

        }

        return $this->render('acteur/modif.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/supp/{id}", name="supp")
     */
    public function supprimer($id){
        $em = $this->getDoctrine()->getManager();
        $acteur = $em->getRepository(Acteur::class)->find($id);
        if(!$acteur){
            throw $this->createNotFoundException('L\'acteur avec l\'id '.$id.' n\'existe pas.');
        }
        $em->remove($acteur);
        $em->flush();

//        return new Response('Acteur supprimé avec succès');
        return $this->redirect('/acteur');

    }

}